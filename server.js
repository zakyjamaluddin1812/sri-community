const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const Router = require("./routes/index");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const PORT = 3000;

app.use(Router);

app.listen(PORT, console.log("Server don start for port: " + PORT));
