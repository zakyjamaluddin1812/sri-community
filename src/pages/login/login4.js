import React from "react";
import { Link } from "react-router-dom";
import Logo from "./logo";
import style from "./style.module.css";

export default function login4() {
  return (
    <div>
      <div>
        <Logo />
      </div>
      <div className={style.h1Login4}>
        <h1>Lupa Kata Sandi?</h1>
        <p>
          {" "}
          Jika anda bersedia kami akan mengirim kode pemulihan lewat email.{" "}
          <br />
          silahkan masukkan email anda!
        </p>
      </div>
      <div className={style.form}>
        <input type="text" placeholder ="  Masukkan email" />
        <div className={style.masuk}>
          <Link to="/login5">Kirim</Link>
        </div>
      </div>
    </div>
  );
}
