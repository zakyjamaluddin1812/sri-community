import React from "react";
import Logo from "./logo";
import style from "./style.module.css";
import { Link } from "react-router-dom";

export default function login5() {
  return (
    <div>
      <div>
        <Logo />
      </div>

      <div className={style.h1Login4}>
        <h1>Lupa Kata Sandi?</h1>
        <p>
          Kami telah mengirim kode pemulihan sandi ke email anda. silahkan
          masukkan kode di kolom dibawah ini
        </p>
      </div>
      <div className={style.form}>
        <input type="text" placeholder="  Masukkan Kode" />
        <div className={style.masuk}>
          <Link to="/login6">Kirim</Link>
        </div>
      </div>
    </div>
  );
}
