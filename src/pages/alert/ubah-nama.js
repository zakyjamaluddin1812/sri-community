import React from "react";
import { Link } from "react-router-dom";
import Navbar from "../beranda/nav";

export default function ubahNnama() {
  return (
    <div>
      <div>
        <Navbar />
      </div>
      <div>
        <h1 className=" font-bold text-lg	leading-5	 p-1.5	pl-3	">Informasi Pengguna</h1>
        <div className="pl-2.5	"><table >
          <tr>
            <td>Nama</td>
            <td className="pl-5	">:</td>
            <td>Nama</td>
          </tr>
          <tr>
            <td>Email</td>
            <td className="pl-5	">:</td>
            <td>zaki@Gmail.com</td>
          </tr>
          <tr>
            <td>Bergabung sejak</td>
            <td className="pl-5	">:</td>
            <td>  1 januari 2000</td>
          </tr>
        </table>
        </div>
      </div>
      <div className="grid w-32	font-bold  m-2.5			">
        <Link to="/" className=" p-2 m-2.5		rounded-lg text-white bg-teal-400">Ubah Nama</Link>
        <Link to="/" className=" p-2 m-2.5		rounded-lg text-white bg-teal-400">Ubah sandi</Link>
      </div>
    </div>
  );

}

