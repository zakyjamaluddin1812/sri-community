import React from "react";
import logo from "../../assets/image/logo.svg";
import Style from "./style.module.css";
import { Link } from "react-router-dom";
export default function login() {
  return (
    <div>
      <div className={Style.parent}>
        <img src={logo} alt="" />
        <h1>Inventaris</h1>
        <p>UNU Sunan Giri Bojonegoro</p>
        <Link to="./login2">Get Started</Link>
      </div>
    </div>
  );
}
