import React from "react";
import Logo from "./logo";
import { Link } from "react-router-dom";
import style from "./style.module.css";
export default function login6() {
  return (
    <div>
      <div>
        <Logo />
      </div>
      <form className={style.form}>
        <input type="text" placeholder="  kata sandi baru" />
        <input type="password" placeholder="  konfirmasi kata sandi baru" />
        <div className={style.masuk}>
          <Link to="/beranda">Kirim</Link>
        </div>
      </form>
    </div>
  );
}
