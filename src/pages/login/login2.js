import React, { useState } from "react";
import Inventaris from "./logo";
import style from "./style.module.css";
import { datauser } from "../data/dataUser";
import { Link, useNavigate } from "react-router-dom";
export default function Login2() {
  const [username, setUsername] = useState("");
  const [hasil, setHasil] = useState("");
  const handleUsername = (e) => {
    setUsername(e.target.value);
    console.log("value is", e.target.value);
    if (e.target.value === datauser.username) {
      console.log("value is", datauser);

      setHasil(true);
    } else {
      setHasil(false);
    }
  };
  const [password, setPassword] = useState("");
  const [hasil2, setHasil2] = useState("")
  const handlePassword = (e) => {
    setPassword(e.target.value);
if(e.target.value === datauser.password){
  setHasil2(true)
}else{
  setHasil2(false)
}

  };


  let navigate = useNavigate();
  const handleClick = () => {
    if (hasil === true && hasil2 === true) {
      navigate("/beranda", { replace: true });
    }else if (hasil === "" && hasil2 === ""){
      alert(" masukkan data");
    } else if (hasil === false){
      alert("username salah");
    }else if (hasil2 === false){
      alert("password salah");
    }else if(hasil === ""){
      alert("masukkan email");
    }else if(hasil2 === ""){
      alert("masukkan password");
    }
  };
  // console.log(hasil);
  console.log(datauser.username);
  return (
    <div>
      <div>
        <Inventaris />
      </div>
      <form className={style.form}>
        <input
          type="email"
          value={username}
          onChange={handleUsername}
          placeholder="    alamat email"
        />
        <input
          type="password"
          value={password}
          onChange={handlePassword}
          placeholder="    kata sandi"
        />
        <div id={style.masuk}>
          <button onClick={handleClick}>Masuk</button>
        </div>
        <div className={style.punyaAkun}>
          <h1>
            Belum punya Akun? <Link to="/login3">Daftar</Link>
            <Link to="/login4" id={style.lupaSandi}>
              Lupa Kata Sandi
            </Link>
          </h1>
        </div>
      </form>
    </div>
  );
}
