
import HalamanLogin from "./pages/login/index";
// import Dashboard from "./pages/beranda/dashboard";
import Login2 from "./pages/login/login2";
import Login3 from "./pages/login/login3";
import Login4 from "./pages/login/login4";
import Login5 from "./pages/login/login5";
import Login6 from "./pages/login/login6";
import Dashboard from "./pages/beranda/dashboard";
import Account  from "./pages/alert/account";
import UbahNnama  from "./pages/alert/ubah-nama";
import Ubah  from "./pages/alert/ubah";
import Password  from "./pages/alert/pasword";
import Logout  from "./pages/alert/logout";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
function App() {
  return (
    
    <BrowserRouter>
    <Routes>
      <Route path="/" element ={<HalamanLogin/>}/>
      
      <Route path="/login2" element ={<Login2/>}/>
      <Route path="/login3" element ={<Login3/>}/>
      <Route path="/login4" element ={<Login4/>}/>
      <Route path="/login5" element ={<Login5/>}/>
      <Route path="/login6" element ={<Login6/>}/>
      <Route path="/beranda" element ={<Dashboard/>}/>
      <Route path="/account" element={<Account/>}/>
      <Route path="/Edit" element={<UbahNnama/>}/>
      <Route path="/ubahnama" element={<Ubah/>}/>
      <Route path="/password" element={<Password/>}/>
      <Route path="/logout" element={<Logout/>}/>

     </Routes>
    </BrowserRouter>
    
  );
}

export default App;
