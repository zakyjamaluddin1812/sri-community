import React from "react";
import style from "./style.module.css"
import logo from "../../assets/image/logo.svg"

export default function Inventaris() {
  return (
    <div className={style.parent2}>
    <div className={style.logo}>
      <img src={logo} alt=""/>
      <h1> Inventaris</h1>
      <p>UNU Sunan Giri Bojonegoro</p>
      </div>
    </div>
  );
}
