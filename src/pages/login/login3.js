import React from "react";
import Logo from "./logo";
import style from "./style.module.css";
import { Link } from "react-router-dom";
export default function login3() {
  return (
    <div>
      <div>
        <Logo />
      </div>
      <form className={style.form}>
        <input type="text" placeholder="  nama" />
        <input type="text" placeholder="  alamat email" />
        <input type="password" placeholder="  kata sandi" />
        <input type="password" placeholder="  konfirmasi kata sandi" />
        <div id={style.masuk}>
          <button>Daftar</button>
        </div>
        <div className={style.punyaAkun}>
          <h1>
            Sudah Punya Akun <Link to="/login2">Masuk</Link>
          </h1>
        </div>
      </form>
    </div>
  );
}
