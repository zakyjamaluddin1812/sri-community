import React from "react";
import {Link } from "react-router-dom";
import Navbar from "./nav";
import id1 from "../../assets/image/imgId/id1.svg";
import id2 from "../../assets/image/imgId/id2.svg";
import id3 from "../../assets/image/imgId/id3.svg";
import id4 from "../../assets/image/imgId/id4.svg";
import id5 from "../../assets/image/imgId/id5.svg";
import id6 from "../../assets/image/imgId/id6.svg";
import id7 from "../../assets/image/imgId/id7.svg";
import cari from "../../assets/image/cari.svg";
import style from "./style.module.css";
export default function nav() {
  return (
    <>
      <div>
        <div>
          <Navbar />
        </div>
        <div className={style.wrapper}>
          <div className={style.parentA}>
            <div id={style.chill}>
              <Link to="/">
              <img src={id1} alt="" />
              </Link>
              <p>elektronik</p>
            </div>
            <div id={style.chill}>
              <Link to="">
              <img src={id2} alt="" />
              </Link>
              <p>kebersihan</p>
            </div>
            <div id={style.chill}>
              <Link to="">
              <img src={id3} alt="" />
              </Link>
              <p>olahraga</p>
            </div>
            <div id={style.chill}>
              <Link to="">
              <img src={id4} alt="" />
              </Link>
              <p>atk</p>
            </div>
            <div id={style.chill}>
              <Link to="">
              <img src={id5} alt="" />
              </Link>
              <p>ruang</p>
            </div>
            <div id={style.chill}>
              <Link to="">
              <img src={id6} alt="" />
              </Link>
              <p>atribut</p>
            </div>
            <div id={style.chill}>
              <Link to="/">
              <img src={id7} alt="" />
              </Link>
              <p>lain-lain</p>
            </div>
          </div>
        </div>
      </div>
      <div className={style.button1}>
        <button id={style.button1}>
          <Link to="/">
          <img src={cari} alt="" /></Link>
        </button>
      </div>
    </>
  );
}
