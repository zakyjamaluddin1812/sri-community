import React from 'react'
import style from "./style.module.css"
export default function logout() {
  return (
    <div className={style.ubah}>
        <div className={style.wrapper}>
            <h1>Logout</h1>
            <h2>apakah anda yakin akan keluar</h2>
            <div className={style.buttonUbah}>
            <button>logout</button>
            </div>
        </div>

    </div>
  )
}
